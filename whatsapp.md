### bold
To **bold** your message, place an asterisk on both sides of the text, like so:
```
*bold*
```

### italics
To *italicize* your message, place an underscore on both sides of the text, like so:
```
_italics_
```

### strikethrough
To ~~strikethrough~~ your message, place a tilde on both sides of the text, like so:
```
~strikethrough~
```


### monospace
To ```monospace``` your message, place three backticks on both sides of the text, like so:
```
```monospace text```
```
