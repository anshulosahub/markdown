# What is markdown
it's a language/ way which makes your text look beautiful and structured.

**Formally Speaking**
> Markdown is a lightweight markup language with plain text formatting syntax. [read more](https://en.wikipedia.org/wiki/Markdown)


# Where is it used?
It's used at different places for different purposes. For eg.

* Translating text to HTML
* Chatting applications
	* WhatsApp
	* Slack
* Create readme files for version control systems
	* Bitbucket
	* github
* JIRA
* jupyter Notebooks

## Purpose of this repository?
To help you with markdown syntax

## Who do I talk to?
anshul@osahub.com