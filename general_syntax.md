# H1 Normal Text

Normal Text

H1 Code Block
==================

```
code block
```

To add syntax highlighting to a code block, add the name of the language immediately
after the backticks: 
```javascript
var oldUnload = window.onbeforeunload;
window.onbeforeunload = function() {
    saveCoverage();
    if (oldUnload) {
        return oldUnload.apply(this, arguments);
    }
};
```

## H2 Text Styles
*Italic1* 

_Italic2_

**bold1**

__bold2__

~~strikethrough~~

H2 Hyperlink
------------------
[osahub website](http://www.osahub.com)

### H3 Normal List

*  Item 1
*  Item 2
*  Item 3
    *  Item 3a
    *  Item 3b
	
#### H4 Numbered List

1.  Step 1
2.  Step 2
3.  Step 3
    1.  Step 3.1
    2.  Step 3.2
    3.  Step 3.3
	
##### H5 Mixed List

1.  Step 1
2.  Step 2
3.  Step 3
    * Step 3.1
    * Step 3.2
    * Step 3.3

>quote : content has been copied from [this page](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)