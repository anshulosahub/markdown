### bold
To **bold** your message, place an asterisk on both sides of the text, like so:
```
*bold*
```

### italics
To *italicize* your message, place an underscore on both sides of the text, like so:
```
_italics_
```

### strike
To ~~strike~~ your message, place a tilde on both sides of the text, like so:
```
~strike~
```


### code
```
`code`
```


### pre-formatted
```
```pre-formatted```
```


### quote
```
>quote
```
